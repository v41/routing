import numpy as np
import time
import math
import random
import timecop #to make dynamic simulation by tick ratio no scaling
import googlemaps
import pandas as pd
from datetime import timedelta
start_time = time.time()

###main parameters
maxCap = 3 #number of vehicle capacity
sRP=15000 #charge per bookings
cPKM=2000 #cost per km (paid to driver)
fPKM=2500 #fare per km (paid by consumer at minimum )
minPay=8 # min km paid to driver
###########

#%timeit to check running time
#nodes=np.genfromtxt('demo2.csv',delimiter=",",dtype='int')
f= open(('D:\Drive\GO\src\maincase\log['+str(maxCap)+'-'+str(sRP)+'-'+time.strftime('%y%W%w%H%M%S')+'].txt'),'w')
path = r'D:\Drive\GO\src\maincase\bookings_100.csv'
#generate first 2000 of bookings with distance above 5km?

def show(string):
    print(string)
    f.write(string+'\n')

raw=pd.read_csv(path,header=0)
olat=raw['origin_lat']
olong=raw['origin_long']
dlat=raw['destination_lat']
dlong=raw['destination_long']
pickup=raw['pickuptime']
distancekm=raw['distance_km']
##Booking confirmed
#raw.insert(raw.shape[1],'status',''))// shape[0] num of rows, 1 col
#raw.set_value(0,'status','CONFIRMED')
#odlat/long: append origin and destination to lat & long
#def odlat(olat,olong,dlat,dlong):
odlat=[]
for i in range(len(raw)):
    odlat.append(olat[i])
    odlat.append(dlat[i])
odlong=[]
for i in range(len(raw)):
    odlong.append(olong[i])
    odlong.append(dlong[i])

#distmatrix
#if 'dm' not in globals():#and len(dm)==len(raw) #to stop recalculation
foo=[]
nodes=[[foo for i in range(len(odlat))]for j in range(len(odlong))]
for i in range(len(odlat)):
    for j in range(len(odlat)):
        nodes[i][j]=110.5 * abs(odlat[j]-odlat[i]) + 111 * abs(odlong[j]-odlong[i])
#nodes=np.array(dm)
numNodes=len(nodes) # includes origin and destinations, number 
maxDrv = math.ceil(numNodes/2/maxCap) #number of drivers based on capacity
totalCost=int(numNodes/2*sRP) #total bookings paid 

#initialization
totalPayable=0
totalRoute=0
totalPaidSend=0
totalInstant=0
current = 1 #initial current as member of tabu e.g. destination
tabu = [] #tabu list to check the current status
origins = [] #
for o in range(0,numNodes,2):
    origins.append(o) 
for n in range(1,numNodes,2):
    tabu.append(n)
for v in range(maxDrv):
    origin=1
    destin=0
    route =[]
    book = []
    while current in tabu: 
        current = random.choice(range(0,numNodes,2))
    if current not in tabu:
        tabu.append(current)
    route= [current]
    curBook =int(current/2)
    book= [curBook]
    if current % 2 == 0 and current + 1 in tabu: #
        tabu.remove(current+1)
    while destin < maxCap:
        best = 1000000 #large initial number
        now = 0
        bestNode = 0    
        for c in range(numNodes):
            if origin < maxCap:
                if c not in tabu:
                    now = nodes[current][c]                
                    if now <= best:
                        best = now
                        bestNode = c
            else:
                if c not in tabu and c not in origins:
                    now = nodes[current][c]                
                    if now <= best: # and now != 0
                        best = now
                        bestNode = c
#put some more rules if it is not a consolidation
        if len(tabu) == numNodes:
            break
        current = bestNode
        route.append(bestNode)
        tabu.append(bestNode)
        if bestNode % 2== 0:
            bookNo=int(bestNode/2)
            book.append(bookNo)
        if current % 2 == 0:
            origin += 1
            if current + 1 in tabu:
                tabu.remove(current+1)
        else:
            destin +=1
    stop = [nodes[r][c] for (r,c) in zip(route,route[1:])]
    routeLen = sum(stop)
##########################
    instant=0
    for i in range(len(book)):
        instant+=distancekm[book[i]]
    totalInstant+=instant
################GOoglemaps
    ranger=[]
    distaCum=[]
    if len(route)>2:
        for i in range(1,(len(route)-1)):
            wp=(str(odlat[route[i]])+','+ str(odlong[route[i]]))
            ranger.append(wp)
    gmaps=googlemaps.Client(key="AIzaSyBWzWHGPlZ9PXbAv7fi823-P2ZrHqCSxSA")
    origindir = odlat[route[0]],odlong[route[0]]
    destindir = odlat[route[len(route)-1]],odlong[route[len(route)-1]]
    directions_result = gmaps.directions(origindir,destindir,waypoints=ranger,alternatives='true',optimize_waypoints='true',avoid='tolls')
    tdist=0
    for i in range(len(route)-1):
        dista=(directions_result[0]['legs'][i]['distance']['value'])/1000
        distaCum.append(dista)
        tdist+=dista
    totalRoute += tdist

    routeo=[]
    wpo=(directions_result[0]['waypoint_order'])
    routeo.append(route[0]) #first destination
    for i in range(len(wpo)): #waypoint optimized by google
        routeo.append(route[wpo[i]+1])        
    routeo.append(route[len(route)-1]) #last destination
############################
    if tdist > 8: #driver cost per km
        payable = int(round(tdist)*cPKM)
    else:
        payable = int(minPay*cPKM)
    totalPayable += int(payable)
############################
    if tdist > 8: #consumer fare per km
        paidSend = int(round(tdist)*fPKM)
    else:
        paidSend = int(minPay*fPKM)
    totalPaidSend += int(paidSend)    
########################trip results 
    show('-----------------------')
    show("trip-"+ str(v+1) +  " : " + str(book))
#   show("car-"+ str(v+1) + " : " + str(route))
    show("car-"+ str(v+1) + " : " + str(routeo)) #google optimized waypoints
    show("dist : " +str(distaCum))
    show("totaldist : " +str(tdist))
    show("instant : " +str(instant))
    show("diff w instant : " +str(instant-tdist))
#######################
#    show("dist : " + str(stop))
#    show("totaldist : " + str(routeLen))
    show("pay  : " + str(payable))
    show("paid : " + str(paidSend))
#######################total results
show("==============================")    
show("total distance      : " + str(totalRoute))
show("total instant       : " + str(totalInstant))
show("total diff w instant: " + str(totalInstant-totalRoute))
show("total pay (driver)  : " + str(totalPayable))
show("total paid(instant) : " + str(totalCost))
show("total paid(sameday) : " + str(totalPaidSend))
show("mean distance       : " + str(totalRoute/maxDrv))    
timeElapsed = timedelta(seconds=(time.time() - start_time))
show("time elapsed: " + str(timeElapsed))

f.close()