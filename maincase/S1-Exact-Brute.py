import pandas as pd
import time
import googlemaps
from datetime import timedelta


routes = []
###############Direct DistanceMatrix csv
dmf = r'D:\Drive\GO\src\maincase\bookings_3.csv'
cities=pd.read_csv(dmf,header=None)
###########################
source = r'D:\Drive\GO\src\maincase\bookings_2000.csv'
raw=pd.read_csv(source,header=0)
gmaps=googlemaps.Client(key="AIzaSyBWzWHGPlZ9PXbAv7fi823-P2ZrHqCSxSA")
#generate first 2000 of bookings with distance above 5km?

def distanceMatrix(odlat,odlong):
    foo=[]
    dm=[[foo for i in range(len(odlat))]for j in range(len(odlong))]
    for i in range(len(odlat)):
        for j in range(len(odlat)):
            dm[i][j]=110.5 * abs(odlat[j]-odlat[i]) + 111 * abs(odlong[j]-odlong[i])
    dm=pd.DataFrame(dm)#faster in array vs list
    return dm

def distanceMatrixGoogle(odlat,odlong):
    foo=[]
    dm=[[foo for i in range(len(odlat))]for j in range(len(odlong))]
    duram=[[foo for i in range(len(odlat))]for j in range(len(odlong))]
    for i in range(len(odlat)):
        for j in range(len(odlat)):
            distance_result=gmaps.distance_matrix((str(odlat[i])+','+str(odlong[i])),(str(odlat[j])+','+str(odlong[j])),avoid='tolls')
            dm[i][j]=distance_result['rows'][0]['elements'][0]['distance']['value']/1000
            duram[i][j]=distance_result['rows'][0]['elements'][0]['duration']['value']
    dm=pd.DataFrame(dm)#faster in array vs list
    duram=pd.DataFrame(duram)
    return dm, duram
    
def directionsGoogle(odlat,odlong,maxCap):    
#    waypoint=[]
#    if len(route)>2:
#        for i in range(1,(len(route)-1)):
#            wp=(str(odlat[route[i]])+','+ str(odlong[route[i]]))
#            waypoint.append(wp)
     directions_result = gmaps.directions(origindir,destindir,waypoints=waypoint,alternatives='true',optimize_waypoints='true',avoid='tolls')
#    distaCum=[]
#    tdist=0
#    for i in range(len(route)-1):
#        dista=(directions_result[0]['legs'][i]['distance']['value'])/1000 
#        distaCum.append(dista)
#        tdist+=dista
#    totalRoute += tdist

        #dissolve path into instant courier (no consolidation pair)

#odlat/long: append origin and destination to lat & long
#def odlat(olat,olong,dlat,dlong):
totalRoute=0
olat=raw['origin_lat']
dlat=raw['destination_lat']
odlat=[]
for i in range(5):#range(len(raw)):
    odlat.append(olat[i])
    odlat.append(dlat[i])

olong=raw['origin_long']
dlong=raw['destination_long']
odlong=[]
for i in range(5):#range(len(raw)):
    odlong.append(olong[i])
    odlong.append(dlong[i])

distancekm=raw['distance_km'] #instant delivery distance
pickup=raw['pickuptime'] #request/pickup time

#####Booking confirmed
#raw.insert(raw.shape[1],'status',''))// shape[0] num of rows, 1 col
#raw.set_value(0,'status','CONFIRMED')

#####distmatrix
#if 'nodes' not in globals():#and len(nodes)==len(raw) #to stop recalculation

def find_paths(node, cities, path, distance):
    # Add waypoint
    path.append(node)

    # Calculate path length from current to previous node
    if len(path) > 1:
        distance += cities[path[-2]][node]
            
    # If path contains all cities and is not a dead end
    if (len(cities) == len(path)):
        global routes
#        print (path, distance)
        f.write(str(path) + str(distance) +'\n')
        routes.append([distance, path])
        return



    # Fork paths for all possible cities not yet used
    for city in cities:
        if (city not in path) and node in cities[city]:
            if city % 2 == 0:
                find_paths(city, cities, list(path), distance)
            if city % 2 == 1 and len(path)>1 and city-1 in path and city-1 != path[-1]: # check if not sequential
                find_paths(city, cities, list(path), distance)
#            if len(path) > 1:
#            if city % 2 == 1 and city-1 in path:
#                find_paths(city, cities, list(path), distance)
#            if city % 2 == 1 and path[-2] % 2 == 0 and path[-2] + 1 != city: #check sequential and divide
#               find_paths(city, cities, list(path), distance)

def directDelivery(cities):
    instant = 0    
    for i in range(0,len(cities),2):
        instant+=cities[i+1][i]
    return instant
#loops the routing maxCap so can produce best routing!
#compare with instant delivery
# for city in range()

if __name__ == '__main__':
    log=str(len(cities)/2)+'-'+time.strftime('%y%W%w%H%M%S')+'].txt'
    f= open(('D:\Drive\GO\src\maincase\log['+log),'w')
#    cities=distanceMatrix(odlat,odlong)
#    cities,duram=distanceMatrixGoogle(odlat,odlong)
    instant=directDelivery(cities)
    print("Instant length     : %s" % instant)
    start_time = time.time()    #routing 
    for i in range(0,len(cities),2):    
        find_paths(i, cities, [], 0)
    routes.sort()       #routing
#    if len(routes) != 0:
    print("Consolidated length: %s" % routes[0][0])
    print("Consolidated route : %s" % routes[0][1])
#    else:
#        print("FAIL!")
    timeElapsed = timedelta(seconds=(time.time() - start_time))
    print(timeElapsed)
    f.close()