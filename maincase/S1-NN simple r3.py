import pandas as pd
import time
import math
import random
import googlemaps
from datetime import timedelta
start_time = time.time()

###main parameters
maxCap = 3 #number of vehicle capacity
sRP=15000 #charge per bookings
cPKM=2000 #cost per km (paid to driver)
fPKM=2500 #fare per km (paid by consumer at minimum )
minPay=8 # min km paid to driver
###########
totalRoute=0
totalPaidSend=0
totalInstant=0
totalPayable=0
###########
#%timeit to check running time
#nodes=np.genfromtxt('demo2.csv',delimiter=",",dtype='int')
log=str(maxCap)+'-'+str(sRP)+'-'+time.strftime('%y%W%w%H%M%S')+'].txt'
f= open(('D:\Drive\GO\src\maincase\log'+log),'w')
path = r'D:\Drive\GO\src\maincase\bookings_100.csv'
raw=pd.read_csv(path,header=0)
#generate first 2000 of bookings with distance above 5km?

def distancematrix(odlat,odlong):
    foo=[]
    dm=[[foo for i in range(len(odlat))]for j in range(len(odlong))]
    for i in range(len(odlat)):
        for j in range(len(odlat)):
            dm[i][j]=110.5 * abs(odlat[j]-odlat[i]) + 111 * abs(odlong[j]-odlong[i])
    dm=pd.DataFrame(dm)#faster in array vs list
    return dm

def show(string):
    print(string)
    f.write(string+'\n')

#odlat/long: append origin and destination to lat & long
#def odlat(olat,olong,dlat,dlong):
olat=raw['origin_lat']
dlat=raw['destination_lat']
odlat=[]
for i in range(len(raw)):
    odlat.append(olat[i])
    odlat.append(dlat[i])

olong=raw['origin_long']
dlong=raw['destination_long']
odlong=[]
for i in range(len(raw)):
    odlong.append(olong[i])
    odlong.append(dlong[i])

distancekm=raw['distance_km'] #instant delivery distance
pickup=raw['pickuptime'] #request/pickup time

#####Booking confirmed
#raw.insert(raw.shape[1],'status',''))// shape[0] num of rows, 1 col
#raw.set_value(0,'status','CONFIRMED')

#####distmatrix
#if 'nodes' not in globals():#and len(nodes)==len(raw) #to stop recalculation
nodes=distancematrix(odlat,odlong)
numNodes=len(nodes) # includes origin and destinations, number 
maxDrv = math.ceil(numNodes/2/maxCap) #number of drivers based on capacity
totalCost=int(numNodes/2*sRP) #total bookings paid 
######initialization
current = 1 #initial current as member of tabu e.g. destination
tabu = [] #tabu list to check the current status
origins = [] #
for i in range(0,numNodes,2):
    origins.append(i) 
for i in range(1,numNodes,2):
    tabu.append(i)

for v in range(maxDrv): #reworked tabulist
    origin=1
    destin=0
    route =[]
    book  =[]
    while current in tabu: #1st check the current=1, so the loop running until it finds unused current
        current = random.choice(range(0,numNodes,2)) #set random current nodes
    if current not in tabu:
        tabu.append(current)
    route =[current]
    curBook=int(current/2)
    book  =[curBook]
    if current % 2 == 0 and current + 1 in tabu: #
        tabu.remove(current+1)
    while destin < maxCap:
        best= 1000000 #large initial number
        now = 0
        bestNode = 0    
        for i in range(numNodes):
            if origin < maxCap:
                if i not in tabu:
                    now = nodes[current][i]                
                    if now <= best:
                        best = now
                        bestNode = i
            else:
                if i not in tabu and i not in origins:
                    now = nodes[current][i]                
                    if now <= best: # and now != 0
                        best = now
                        bestNode = i
        if len(tabu) == numNodes:
            break
        current = bestNode
        route.append(bestNode)
        tabu.append(bestNode)
        if bestNode % 2== 0:
            bookNo=int(bestNode/2)
            book.append(bookNo)
        if current % 2 == 0:
            origin += 1
            if current + 1 in tabu:
                tabu.remove(current+1)
        else:
            destin +=1
################break the inefficient
#if routeo[i+1]-routeo[i]==1:
#   routeo.remove(i)
#   routeo.remove(i+1)
################bookings instant distance
    instant=0
    for i in range(len(book)):
        instant+=distancekm[book[i]]
    totalInstant+=instant
################Google Directions API for final route
    distaCum=[]
    tdist=0
    origindir = odlat[route[0]],odlong[route[0]]
    destindir = odlat[route[len(route)-1]],odlong[route[len(route)-1]]
    waypoint=[]
    if len(route)>2:
        for i in range(1,(len(route)-1)):
            wp=(str(odlat[route[i]])+','+ str(odlong[route[i]]))
            waypoint.append(wp)
    gmaps=googlemaps.Client(key="AIzaSyBWzWHGPlZ9PXbAv7fi823-P2ZrHqCSxSA")
    directions_result = gmaps.directions(origindir,destindir,waypoints=waypoint,alternatives='true',optimize_waypoints='true',avoid='tolls')
##################Calculate the distance legs, and total trip distance
    for i in range(len(route)-1):
        dista=(directions_result[0]['legs'][i]['distance']['value'])/1000 
        distaCum.append(dista)
        tdist+=dista
    totalRoute += tdist
################populate route and optimized waypoint sequence
    routeo=[]
    wpo=(directions_result[0]['waypoint_order'])
    routeo.append(route[0]) #first destination
    for i in range(len(wpo)): #waypoint optimized by google
        routeo.append(route[wpo[i]+1])        
    routeo.append(route[len(route)-1]) #last destination
############################
    if tdist > 8: #driver cost per km
        payable = int(round(tdist)*cPKM)
    else:
        payable = int(minPay*cPKM)
    totalPayable += int(payable)
############################
    if tdist > 8: #consumer fare per km
        paidSend = int(round(tdist)*fPKM)
    else:
        paidSend = int(minPay*fPKM)
    totalPaidSend += int(paidSend)    
########################trip results 
    show('-----------------------')
    show("trip-"+ str(v+1) +  " : " + str(book))
#   show("car-"+ str(v+1) + " : " + str(route))
    show("car-"+ str(v+1) + " : " + str(routeo)) #google optimized waypoints
    show("dist     : " +str(distaCum))
    show("totaldist     : " +str(tdist))
    show("instant       : " +str(instant))
    show("diff w instant: " +str(instant-tdist))
#######################
#    show("dist : " + str(stop))
#    show("totaldist : " + str(routeLen))
    show("pay           : " + str(payable))
    show("paid          : " + str(paidSend))
#######################total results
show("==============================")    
show("total distance      : " + str(totalRoute))
show("total instant       : " + str(totalInstant))
show("total diff w instant: " + str(totalInstant-totalRoute))
show("total pay (driver)  : " + str(totalPayable))
show("total paid(instant) : " + str(totalCost))
show("total paid(sameday) : " + str(totalPaidSend))
show("mean distance       : " + str(totalRoute/maxDrv))    
timeElapsed = timedelta(seconds=(time.time() - start_time))
show('-----------------------')
show("time elapsed: " + str(timeElapsed))

f.close()